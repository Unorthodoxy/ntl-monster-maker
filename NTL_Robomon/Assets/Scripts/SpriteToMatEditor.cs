﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(SpriteToMat))]
public class SpriteToMatEditor : Editor
{
    public override void OnInspectorGUI()
    {
        //base.OnInspectorGUI();

        DrawDefaultInspector();

        SpriteToMat script = (SpriteToMat)target;
        if (GUILayout.Button("Convert"))
        {
            script.Convert();
        }
        if (GUILayout.Button("Apply"))
        {
            script.Apply();
        }
    }
}
// 