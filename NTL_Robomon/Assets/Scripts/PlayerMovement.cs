﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    Camera cam;
    Pathfinding pathing;
    Grid grid;

    GameObject selectedActor;

    private void Awake()
    {
        cam = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>();
        pathing = GetComponent<Pathfinding>();
        grid = GetComponent<Grid>();
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            GameObject clicked = GetClickObject();

            if (clicked != null)
            {
                if (clicked.tag == "Player" || clicked.tag == "Enemy")
                {
                    // TODO: Tell UI Manager that this player is the active character
                    // TODO: SFX, probably
                    // TODO: Highlight Player
                    // TODO: Highlight reachable tiles (considering move speed)
                    pathing.obj = clicked;  // Setting the active pathfinding agent to this player
                    selectedActor = clicked;
                }
                /*else if (clicked.tag == "Enemy")
                {
                    // TODO: Tell UI Manager to display this character's stats
                    // TODO: SFX, probably
                    // TODO: Highlight Player
                    selectedActor = clicked;
                }*/
                else if ((clicked.tag == "Ground" || clicked.tag == "NavNode") && pathing.obj != null)
                {
                    if (grid.NodeFromWorldPosition(GetMousePosWorld()) == grid.NodeFromWorldPosition(pathing.target))
                    {
                        // "If a player-aligned character is selected, has an ActorMovement script, and the selected space is withing the actor's movement range;
                        if(/*selectedActor.tag == "Player" && */selectedActor.GetComponent<ActorMovement>() != null && pathing.FindPath(selectedActor, clicked.transform.position).Count <= selectedActor.GetComponent<ActorStats>().movement)
                        {
                            grid.NodeFromWorldPosition(selectedActor.transform.position).isOccupiedBy = null;
                            grid.NodeFromWorldPosition(clicked.transform.position).isOccupiedBy = selectedActor.tag;
                            selectedActor.GetComponent<ActorMovement>().MoveAlongPath(pathing.FindPath(selectedActor, clicked.transform.position));
                        }
                    }
                    else
                    {
                        // TODO: SFX, probably
                        // TODO: Highlight tile
                        pathing.target = GetMousePosWorld();
                    }
                }
            }
        }
    }

    public Vector3 GetMousePosWorld()
    {
        RaycastHit hit;
        Ray ray = cam.ScreenPointToRay(Input.mousePosition);
        Vector3 v;

        if (Physics.Raycast(ray, out hit))
            v = hit.point;
        else
            v = Vector3.zero;

        return v;
    }

    public GameObject GetClickObject()
    {
        RaycastHit hit;
        Ray ray = cam.ScreenPointToRay(Input.mousePosition);
        GameObject g;

        if (Physics.Raycast(ray, out hit))
            g = hit.transform.gameObject;
        else
            g = null;

        return g;
    }
}