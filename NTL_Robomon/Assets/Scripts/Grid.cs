﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Grid : MonoBehaviour
{
    public Vector2 gridSize = new Vector2(7,7);
    List<Node> unsortedNodes;
    Node[,] node;

    private void Start()
    {
        unsortedNodes = FindNodes();
        node = SortToGrid(unsortedNodes);
        Debug.Log(NodeFromWorldPosition(new Vector3(2, 0, -1)));
    }

    List<Node> FindNodes()
    {
        List<Node> n = new List<Node>();

        foreach (GameObject g in GameObject.FindGameObjectsWithTag("NavNode"))
        {
            n.Add(g.GetComponent<Node>());
        }

        return n;
    }

    Node[,] SortToGrid(List<Node> u)
    {
        Node[,] n = new Node[100,100];

        u.Sort(CompareVectors);

        int i = 0;

        for(int y = 0; y < gridSize.y; y++)
        {
            for (int x = 0; x < gridSize.x; x++)
            {
                n[x, y] = u[i];
                i++;
            }
        }

        /*for (int y = 0; y < gridSize.y; y++)
        {
            for (int x = 0; x < gridSize.x; x++)
            {
                Debug.Log(n[x, y].transform.position);
            }
        }*/

        return n;
    }

    public static int CompareVectors(Node n1, Node n2)
    {
        Vector3 v1 = n1.transform.position;
        Vector3 v2 = n2.transform.position;

        if (v1 == v2) return 0;

        if (Mathf.Approximately(v1.x, v2.x))
        {
            /*if (Mathf.Approximately(v1.z, v2.z))
                return v1.y > v2.y ? -1 : 1;
            else*/
                return v1.z > v2.z ? -1 : 1;
        }
        return v1.x > v2.x ? -1 : 1;
    }

    public Node NodeFromWorldPosition(Vector3 pos)
    {
        Vector3 gridOffset = node[0, 0].transform.position;

        /*float xPoint = (pos.x - gridOffset.x);
        float yPoint = (pos.y - gridOffset.y);

        xPoint = Mathf.Clamp01(xPoint);
        yPoint = Mathf.Clamp01(yPoint);

        int x = Mathf.RoundToInt(xPoint);
        int y = Mathf.RoundToInt(yPoint);*/

        Collider closest = new Collider();
        foreach(Collider c in Physics.OverlapSphere(pos, 2))
        {
            if((closest == null || (pos-c.transform.position).magnitude < (pos - closest.transform.position).magnitude) && c.tag == "NavNode")
            {
                closest = c;
            }
        }

        return closest.GetComponent<Node>();
    }
}