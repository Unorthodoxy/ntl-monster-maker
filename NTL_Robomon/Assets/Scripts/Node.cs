﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class Node : MonoBehaviour
{
    // The search radius of this node to find other walkable nodes. A radius of 1 allows nodes up to 1u horizontally and 0.5u vertically to be found, but not diagonal nodes
    public float radius = 1;

    // Neighboring nodes (fallable are at least 1u lower than this node, and are essentially one-way connections. Fallable nodes can be farther than 1u away from this node, but only vertically)
    Collider[] walkable;
    Collider[] fallable;
    public List<Node> travelable; // A combination of walkable and fallable

    // Costs and parent notation; set by the actual pathfinder whenever it calculates a path involving this node. Wiped afterwards.
    public int gCost;
    public int hCost;
    public int fCost { get { return gCost + hCost; } }
    public Node parent;

    public string isOccupiedBy = null;

    Grid grid;

    private void Start()
    {
        grid = GameObject.Find("Game Manager").GetComponent<Grid>();
        walkable = GetAdjacent();
        fallable = GetLower();
        foreach(Collider c in walkable) { travelable.Add(c.GetComponent<Node>()); }
        foreach (Collider c in fallable) { if(c != null) travelable.Add(c.GetComponent<Node>()); }
    }

    private void Update()
    {
        if (grid.GetComponent<Pathfinding>().showNavGrid)
        {
            for (int i = 0; i < walkable.Length; i++)
                Debug.DrawLine(transform.position, walkable[i].transform.position, Color.green);
            for (int i = 0; i < fallable.Length; i++)
                if (fallable[i] != null) Debug.DrawLine(transform.position, fallable[i].transform.position, Color.yellow);
        }
    }

    public Collider[] GetAdjacent()
    {
        int layerId = this.gameObject.layer;
        int layerMask = 1 << layerId;

        Collider[] coll = Physics.OverlapSphere(transform.position, radius, layerMask);

        return coll;
    }

    private Collider[] GetLower()
    {
        Collider[] coll = new Collider[4];
        
        RaycastHit hit;
        int i = 0;

        Physics.Raycast(transform.position + Vector3.forward + Vector3.up, Vector3.down, out hit);
        if (hit.collider != null && hit.collider.tag == "NavNode" && !walkable.Contains(hit.collider)) { coll[i] = hit.collider; i++; }
        Physics.Raycast(transform.position - Vector3.forward + Vector3.up, Vector3.down, out hit);
        if (hit.collider != null && hit.collider.tag == "NavNode" && !walkable.Contains(hit.collider)) { coll[i] = hit.collider; i++; }
        Physics.Raycast(transform.position + Vector3.left + Vector3.up, Vector3.down, out hit);
        if (hit.collider != null && hit.collider.tag == "NavNode" && !walkable.Contains(hit.collider)) { coll[i] = hit.collider; i++; }
        Physics.Raycast(transform.position + Vector3.right + Vector3.up, Vector3.down, out hit);
        if (hit.collider != null && hit.collider.tag == "NavNode" && !walkable.Contains(hit.collider)) { coll[i] = hit.collider; i++; }

        return coll;
    }
}
