﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pathfinding : MonoBehaviour
{
    Grid grid;
    public GameObject obj;
    public Vector3 target;

    public bool showNavGrid = false;

    void Awake()
    {
        grid = GetComponent<Grid>();
    }

    private void Update()
    {
        if (obj != null)
        {
            foreach (Node n in FindPath(obj, target))
            {
                Debug.DrawLine(n.transform.position + new Vector3(0, 0.5f, 0), n.parent.transform.position + new Vector3(0, 0.5f, 0));
            }
        }
    }

    public List<Node> FindPath(GameObject actor, Vector3 end)
    {
        Node startNode = grid.NodeFromWorldPosition(actor.transform.position);
        Node endNode = grid.NodeFromWorldPosition(end);

        List<Node> openList = new List<Node>();
        HashSet<Node> closedList = new HashSet<Node>();

        openList.Add(startNode);

        while(openList.Count > 0)
        {
            Node currentNode = openList[0];
            for(int i = 1; i < openList.Count; i++)
            {
                if(openList[i].fCost < currentNode.fCost || openList[i].fCost == currentNode.fCost && openList[i].hCost < currentNode.hCost)
                {
                    currentNode = openList[i];
                }
            }
            openList.Remove(currentNode);
            closedList.Add(currentNode);

            if(currentNode == endNode)
            {
                return GetFinalPath(startNode, endNode);
            }

            foreach(Node n in currentNode.travelable)
            {
                // This IF statement is responsible from stopping opposing factions from walking through eachother
                // It essentially makes sure the space is either empty, or contains a friendly unit
                // If not, it goes right in the ClosedList without any other processing
                if(n.isOccupiedBy != "" && n.isOccupiedBy != null && n.isOccupiedBy != actor.tag) closedList.Add(n);

                if (closedList.Contains(n)) continue;

                // --- //   // This should, theoretically, make a tile that's higher than the current tile cost slightly more, and therefor bias the solution towards same-level or lower-level tiles when the cost would otherwise be the same
                int moveCost;

                if (n.transform.position.y - currentNode.transform.position.y > 0.5f)
                {
                    moveCost = currentNode.gCost + GetManhattenDistance(currentNode, n) + 1;
                }
                else
                    moveCost = currentNode.gCost + GetManhattenDistance(currentNode, n);
                // --- //
                // VV This is the old version, ignoring elevation entirely VV
                // int moveCost = currentNode.gCost + GetManhattenDistance(currentNode, n);

                if (moveCost < n.gCost || !openList.Contains(n))
                {
                    n.gCost = moveCost;
                    n.hCost = GetManhattenDistance(n, endNode);
                    n.parent = currentNode;

                    if (!openList.Contains(n)) openList.Add(n);
                }
            }
        }


        Debug.Log("Pathfinding Failed! Uh-oh!");
        return null;
    }

    List<Node> GetFinalPath(Node start, Node end)
    {
        List<Node> finalPath = new List<Node>();
        Node currentNode = end;

        while(currentNode != start)
        {
            finalPath.Add(currentNode);
            currentNode = currentNode.parent;
        }

        finalPath.Reverse();

        return finalPath;
    }

    int GetManhattenDistance(Node a, Node b)
    {
        int ix = Mathf.RoundToInt(Mathf.Abs(a.transform.position.x - b.transform.position.x));
        int iy = Mathf.RoundToInt(Mathf.Abs(a.transform.position.z - b.transform.position.z));

        return ix + iy;
    }
}
