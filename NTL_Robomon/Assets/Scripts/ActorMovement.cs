﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Pixelplacement;

public class ActorMovement : MonoBehaviour
{
    int nextNode = 0;
    List<Node> nodesToFollow;
    public GameObject movementSpline;
    public float nodeToNodeTime = 0.25f;
    public float fallTime = 0.1f;
    ActorStats stats;

    private void Start()
    {
        stats = GetComponent<ActorStats>();
    }

    // Call this function to start the movement animation
    public void MoveAlongPath(List<Node> n)
    {
        nodesToFollow = n;
        nextNode = 0;
        MoveToNextNode();
    }

    // Call this function to begin each step of the movement (this function essentially calls itself thru the RemoveSpline coroutine, and stops once the end of the movement is reached)
    public void MoveToNextNode()
    {
        if (nextNode < nodesToFollow.Count)
        {
            Node n = nodesToFollow[nextNode];

            Spline spl = Instantiate(movementSpline, transform.position, new Quaternion(0, 0, 0, 0)).GetComponent<Spline>();

            spl.Anchors[0].transform.position = transform.position;
            spl.Anchors[1].transform.position = n.transform.position;

            float disp = 0;

            if (spl.Anchors[0].transform.position.y > spl.Anchors[1].transform.position.y)
            {
                spl.Anchors[1].InTangent.transform.position = new Vector3(spl.Anchors[1].InTangent.transform.position.x, spl.Anchors[0].OutTangent.transform.position.y, spl.Anchors[1].InTangent.transform.position.z);
                disp = spl.Anchors[0].transform.position.y - spl.Anchors[1].transform.position.y;

            }

            Tween.Spline(spl, gameObject.transform, 0, 1, false, nodeToNodeTime + (disp * fallTime * nodeToNodeTime), 0);
            nextNode++;
            StartCoroutine(RemoveSpline(spl, nodeToNodeTime + (disp * fallTime * nodeToNodeTime)));
        }
    }

    IEnumerator RemoveSpline(Spline spl, float wait)
    {
        yield return new WaitForSeconds(wait);
        MoveToNextNode();
        Destroy(spl.gameObject);
    }
}
