﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEngine.UI;

public class SpriteToMat : MonoBehaviour
{
    public Texture2D spriteToConvert;
    
    int count;
    List<string> sliceNames = new List<string>();

    public void Convert()
    {
        object[] sprites = AssetDatabase.LoadAllAssetRepresentationsAtPath(AssetDatabase.GetAssetPath(spriteToConvert));

        SpriteMetaData[] sliceMeta = (AssetImporter.GetAtPath(AssetDatabase.GetAssetPath(spriteToConvert)) as TextureImporter).spritesheet;
        sliceNames.Clear();
        foreach(SpriteMetaData smd in sliceMeta)
        {
            sliceNames.Add(smd.name);
        }

        Debug.Log(sprites.Length);
        count = sprites.Length;

        int i = 0;

        if(!AssetDatabase.IsValidFolder("Assets/Materials/" + spriteToConvert.name))
            AssetDatabase.CreateFolder("Assets/Materials", spriteToConvert.name);

        foreach (object o in sprites)
        {
            Sprite s = null;

            if (o.GetType() == typeof(Sprite))
            {
                s = o as Sprite;
            }

            Debug.Log(s.name);

            var croppedTexture = new Texture2D((int)s.rect.width, (int)s.rect.height);
            var pixels = s.texture.GetPixels((int)s.textureRect.x, (int)s.textureRect.y, (int)s.textureRect.width, (int)s.textureRect.height);
            croppedTexture.SetPixels(pixels);
            croppedTexture.Apply();

            // croppedTexture.filterMode = FilterMode.Point;
            
            string sPath = Application.dataPath + "/Materials/" + spriteToConvert.name + "/" + s.name + ".png";

            // s.texture.filterMode = FilterMode.Point;

            Texture2D unCom = new Texture2D(croppedTexture.width, croppedTexture.height, TextureFormat.ARGB32, false);
            unCom.SetPixels(0, 0, croppedTexture.width, croppedTexture.height, croppedTexture.GetPixels());
            byte[] imgBytes = unCom.EncodeToPNG();
            System.IO.File.WriteAllBytes(sPath, imgBytes);

            //StartCoroutine(ApplyToMat("Assets/Materials/" + spriteToConvert.name + "/" + i));

            i ++;
        }
    }

    public void Apply()
    {
        int i = 0;

        do
        {
            if (!System.IO.File.Exists(Application.dataPath + "/Materials/" + spriteToConvert.name + "/" + sliceNames[i] + ".mat"))
            {
                Material m = new Material(Shader.Find("HDRP/Lit"));

                Texture2D savedImg = (Texture2D)AssetDatabase.LoadAssetAtPath("Assets/Materials/" + spriteToConvert.name + "/" + sliceNames[i] + ".png", typeof(Texture2D));
                savedImg.filterMode = FilterMode.Point;

                m.SetFloat("_Metallic", 1);
                m.SetFloat("_Smoothness", 0);
                m.SetTexture("_BaseColorMap", savedImg);

                AssetDatabase.CreateAsset(m, "Assets/Materials/" + spriteToConvert.name + "/" + sliceNames[i] + ".mat");
                AssetDatabase.SaveAssets();
            }

            i++;
        }
        while (i<=count + 1);
    }
}